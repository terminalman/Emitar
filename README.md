# Emitar

Universally useable firmware for basic Flashlights with a single soft switch.

**Supported:**
* Emisar D1
* Emisar D4
<br>
<br>

The software is portable to platforms that offer the following features:

**8-Bit timer one offering:**
* Overflow interrupt
<br>
<br>

**8-Bit timer two offering:**
* Two independent PWM outputs
* Overflow interrupt
<br>
<br>

**Miscellaneous:**
* External Interrupt
* 2kb of Flash
* Temperature Sensor
* ADC
