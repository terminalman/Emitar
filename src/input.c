#include <stdint.h>

#include "hal.h"

extern volatile uint8_t button_event;

void en_switch_iface()
{
	EN_SWITCH_IFACE();
}

uint8_t return_button_event()
{
	uint8_t tmp = button_event;
	button_event = 0;
	return tmp;
}


