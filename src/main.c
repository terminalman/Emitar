#include <stdint.h>

#include "output.h"
#include "temp.h"
#include "input.h"
#include "voltage.h"

// includes for testing only
#include "global.h" 
#include <util/delay.h>
#include "debug.h"
#include <stdio.h>


int main()
{
	en_switch_iface();

	debug_port_high();
	
	usart_setup();	

	int16_t volt = return_bat_v();	
	uint8_t buf[7] = {'\0'};
	sprintf((char*)buf,"%d",volt);
	usart_write_string(buf);

	while(1){

		if(return_button_event() == 1){
			debug_port_low();
			set_brightness(0x05);
		}
		if(return_button_event() == 2){
			debug_port_high();
			set_brightness(0xff);
		}
		
	}

	return 0;
}
