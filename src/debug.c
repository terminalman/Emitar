#include <stdint.h>

#include "debug.h"
#include "hal.h"

void usart_setup()
{
	USART_SETUP();
}

void usart_write_char( uint8_t data )
{
	USART_WRITE_CHAR(data);
}

void usart_write_string( uint8_t *data){
        for(int i=0;data[i]!='\0';i++){
                USART_WRITE_CHAR(data[i]);
        }
}

void debug_port_high()
{
	DEBUG_PORT_HIGH();
}

void debug_port_low()
{
	DEBUG_PORT_LOW();
}


