#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#include "hal.h"
#include "global.h"

#define UBRR (F_CPU/16/BAUD-1)

//internal bandgap reference in mV
#define IBGRV 1100

//battery sens voltage divider
#define VDVDR 2.03

/*
	PB0 -> Debug LED Port
	PB1 -> Switch input
	PD5 -> Driver PWM output
	PD6 -> Driver PWM output
	PD1 -> USART TX
	PC0 -> ADC in (Battery voltage)
*/



/* 
 ****************************************************************************************
 ****************************************************************************************
 ***      USART     
 ****************************************************************************************
 ****************************************************************************************
 */
void USART_SETUP()
{
 	/*Set baud rate */
 	UBRR0H = (uint8_t)(UBRR>>8);
 	UBRR0L = (uint8_t)UBRR;

 	/*Enable receiver and transmitter */
 	UCSR0B = (1<<RXEN0)|(1<<TXEN0);

	/* Set frame format: 8data, 2stop bit */
 	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}

void USART_WRITE_CHAR( uint8_t data )
{
    while ( !( UCSR0A & (1<<UDRE0)) );
    UDR0 = data;
}



/*
 ****************************************************************************************
 ****************************************************************************************
 ***      DEBUG PORT      
 ****************************************************************************************
 ****************************************************************************************
 */
void DEBUG_PORT_HIGH()
{
	DDRB |= (1 << PB0);
	PORTB |= (1 << PB0);
}

void DEBUG_PORT_LOW()
{
	DDRB |= (1 << PB0);
	PORTB &= ~(1 << PB0);
}



/*
 *********************
 ***     Input     ***
 *********************
 */

//button functions
void EN_SWITCH_IFACE()
{
	DDRB &= ~(1 << PB1);
	PORTB |= (1 << PB1);
	PCICR |= (1 << PCIF0);
	PCMSK0 |= (1 << PCINT1);
	sei();
}

//unpressed Vcc, pressed Vdd due to the internal pullup resistor
uint8_t SWITCH_STATE_READ()
{
	if( PINB & (1 << PB1) ) return 0;
	else return 1;
}

// timer used to measure button press length
void TIMER_STATE_SET(uint8_t x)
{
	if(x){
		TCCR2B = (1<<0) | (1<<1) | (1<<2); //1024 clock divider
		TCNT2 = 0;
		TIMSK2 = (1<<0); //overflow interrupt enable
		sei();
	}
	else{
		TCCR2B &= ~(1<<0) & ~(1<<1) & ~(1<<2);
	}
}



/* 
 ****************************************************************************************
 ****************************************************************************************
 ***      ADC      
 ****************************************************************************************
 ****************************************************************************************
 */

//Returns the internally measured Temperature
int8_t RETURN_TEMP()
{
	// ADEN => ADC enable
	ADCSRA = 0;
	ADCSRA |= (1<<ADEN);

	// REFS1 REFS0 => internal 1.1V reference
	// MUX3 => Tempsensor
	ADMUX = 0;
	ADMUX |= (1<<REFS1) | (1<<REFS0) | (1<<MUX3);

	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));

	return (ADC - 295) * 1100 / 1024;
}

//Return the Voltage source voltage, not the sensed one. i.e. V_sens * VDVDR
uint16_t RETURN_BAT_V()
{
	ADCSRA = 0;
	ADCSRA |= (1<<ADEN);

	// REFS1 REFS0 => internal 1.1V reference
	// MUX[3:0] = 0 => Channel 0
	ADMUX = 0;
	ADMUX |= (1<<REFS1) | (1<<REFS0);

	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
	
	return (uint32_t)((long)ADC*1100/1024*((uint32_t)(VDVDR*100))/100);
}



/* 
 ****************************************************************************************
 ****************************************************************************************
 ***      PWM      
 ****************************************************************************************
 ****************************************************************************************
 */

// set the PWM output 0-255 for OCR0A and OCR0B
void PWM_STATES_SET(uint8_t a, uint8_t b)
{
	// Set PD5/6 as outputs.
	DDRD |= (1 << PD5) | (1 << PD6);

	// Set Fast PWM mode.
	TCCR0A |= (1 << WGM00) | (1 << WGM01);
	
	if(!a && !b){
		// disable clock to save energy
		TCCR0B &= ~(1<<CS01);
	}
	else{
		// prescaler 8
		TCCR0B |= (1 << CS01);
	}
	if(!a){
		TCCR0A &= ~(1<<COM0A1);
		PORTD &= ~(1<<PD5);
	}
	if(!b){
		TCCR0A &= ~(1<<COM0B1);
		PORTD &= ~(1<<PD6);
	}
	if(a){
		OCR0A = a;
		TCCR0A |= (1 << COM0A1);
	}
	if(b){
		OCR0B = b;
		TCCR0A |= (1 << COM0B1);
	}
}

/*
// alternative function. runs on a 16bit timer, which might not be available
// on all platforms. also likely too slow + likely unnecessary
void set_pwm_states(uint16_t a, uint16_t b)
{
	// Set PB1/2 as outputs.
	DDRB |= (1 << DDB1) | (1 << DDB2);

	// Set OC1A/OC1B on Compare Match, Fast PWM mode.
	TCCR1A = (1 << WGM11);
	// Fast PWM mode.
	TCCR1B = (1 << WGM12) | (1 << WGM13);
	OCR1A = 0;

	// Set the counter value that corresponds to
	// full duty cycle. For 15-bit PWM use
	// 0x7fff, etc. A lower value for ICR1 will
	// allow a faster PWM frequency.
	ICR1 = 0xffff;

	if(!a && !b){
		TCCR1B &= ~(1<<CS10);
	}
	// set the output to zero if disconnected from PWM
	if(!a){
		TCCR1A &= ~(1<<COM1A1);
		PORTB &= ~(1<<PB1);
	}
	if(!b){
		TCCR1A &= ~(1<<COM1B1);
		PORTB &= ~(1<<PB2);
	}
	if(a){
		TCCR1A |= (1<<COM1A1);	
		TCCR1B |= (1<<CS10);
		OCR1A = a;		
	}	
	if(b){
		TCCR1A |= (1<<COM1B1);	
		TCCR1B |= (1<<CS10);
		OCR1B = b;
	}	
}
*/
