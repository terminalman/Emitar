#include <avr/interrupt.h>

#include "global.h"
#include "hal.h"

volatile uint8_t timer_ovf = 0;
volatile uint8_t button_event = 0;

const uint8_t short_press_ovf = (uint8_t) ( F_CPU / 1024 / 256 * short_press / 1000);
const uint8_t long_press_ovf =  (uint8_t) ( F_CPU / 1024 / 256 * long_press / 1000);

ISR(PCINT0_vect){
	if (SWITCH_STATE_READ()){
		TIMER_STATE_SET(1); //start timer
	}
	else{
		TIMER_STATE_SET(0); //stop timer
		if(timer_ovf > short_press_ovf) button_event = 1;
		if(timer_ovf > long_press_ovf) button_event = 2;
		timer_ovf = 0;
	}	
}

ISR(TIMER2_OVF_vect){
	if(timer_ovf < long_press_ovf+1) timer_ovf++;
	if(timer_ovf == long_press_ovf) button_event = 2;
}
