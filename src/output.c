#include <stdint.h>

#include "output.h"
#include "hal.h"

//takes 0-255 -> channel distribution still bad
void set_brightness(uint8_t x)
{
	if(x == 0){
		PWM_STATES_SET(0,0);
	}
	else{
		PWM_STATES_SET( (x<52) ? (x*5) : 0xff, (x>51) ? (x-51) : 0x00 );
	}
}

// takes brightness lvl x and strobe frequency y
void strobe_mode(uint8_t x, uint8_t y)
{
	//use the PWM Timer0 ovf_vect to generate the strobe frequency
	//tricky because when set_brightness(0) the timer stops.
}

