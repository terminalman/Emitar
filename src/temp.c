#include <stdint.h>

#include "hal.h"

int8_t return_temp()
{
	int16_t tmp=0;
	for(int i=0;i<5;i++){
		tmp += RETURN_TEMP();
	}
	tmp /=5;
	return tmp;
}

