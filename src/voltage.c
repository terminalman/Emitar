#include <stdint.h>

#include <hal.h>
#include <voltage.h>

uint16_t return_bat_v()
{
	return RETURN_BAT_V();
}
