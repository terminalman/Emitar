// sets the LED output brightness 0-255
void
	set_brightness(uint8_t);

void
	strobe_mode(
		uint8_t, // brightness
		uint8_t  // frequency
		);
