// 0 = no event, 1 = short press, 2 = long press
uint8_t
	return_button_event();

// enables the switch interface
// by setting this the switch activations are measured via an timer interrupt
// the resulting presses can be read via looping return_button_event();
void
	en_switch_iface();


