/*
 *********************
 *** miscellaneous ***
 *********************
 */

void
	DEBUG_PORT_HIGH();

void
	DEBUG_PORT_LOW();



/*
 *********************
 ***     Input     ***
 *********************
 */

// enable input pin for external switch
void
	EN_SWITCH_IFACE();

// reads the button state debounces and returns either
// 0 for not pressed
// 1 for pressed
uint8_t
	SWITCH_STATE_READ();



/* 
 *********************
 ***      ADC      ***
 *********************
 */

// returns the uC temperature in celsius 20C -> 20
int8_t
	RETURN_TEMP();

// returns the battery voltage in mili volt 4V -> 400
uint16_t
	RETURN_BAT_V();



/* 
 *********************
 ***      PWM      ***
 *********************
 */

void
	PWM_STATES_SET(
		uint8_t, // set power pwm a (0-255)
		uint8_t  // set power pwm b (0-255) 
		);



/* 
 *********************
 ***     TIMER     ***
 *********************
 */

// activate the timer. used to measure button event length
void
	TIMER_STATE_SET(uint8_t);



/* 
 *********************
 ***     USART     ***
 *********************
 */

void
	USART_SETUP();

void
	USART_WRITE_CHAR(uint8_t);


