void usart_setup();

void usart_write_char( uint8_t );

void usart_write_string( uint8_t* );

// these two functions allow the setting/ clearing of the debug pin
void
	debug_port_high();

void
	debug_port_low();

