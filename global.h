#define F_CPU 14745600UL

// minimum press time in ms.
#define long_press 1000
#define short_press 100

#define BAUD 9600
