VERSION = 0.1

# avr-gcc variables
MCU	= atmega328p
ENV 	= dev

#avrdude variables
MCU2	= m328p
PRGRMR	= jtag2isp
PORT	= usb

# # # # # # # # # # # # # # # # # # # #
 # # Do not change anything below  # #
# # # # # # # # # # # # # # # # # # # #

CC	= /usr/bin/avr-gcc
AO	= /usr/bin/avr-objcopy
OUTPUT  = firmware

CFLAGS 	= -Wall -std=c99 -mmcu=$(MCU)

BDIR = buildfolder/
ODIR = $(BDIR)obj/
SDIR = src/
HAL = hal/
IDIR = src/incl/
IDIR2 = .

_OBJK = $(ENV)-$(MCU).o\
	$(ENV)-$(MCU)-isr.o\
	main.o\
	output.o\
	temp.o\
	input.o\
	voltage.o\
	debug.o\
	
OBJK = $(patsubst %,$(ODIR)%,$(_OBJK))

all:	folder $(OUTPUT).hex

folder:
	@-mkdir -p $(ODIR)

clean:
	@-rm buildfolder -r

$(OUTPUT).hex : $(OBJK)
	@-$(CC) $(CFLAGS) -o $(BDIR)$(OUTPUT).elf $(OBJK)
	@-$(AO) $(DFLAGS) $(BDIR)$(OUTPUT).elf buildfolder/$(OUTPUT).hex

$(ODIR)%.o : $(SDIR)%.c
	@-$(CC) $(CFLAGS) -I$(IDIR) -I$(IDIR2) -Os -c $< -o $@

$(ODIR)%.o : $(SDIR)$(HAL)%.c
	@-$(CC) $(CFLAGS) -I$(IDIR) -I$(IDIR2) -Os -c $< -o $@

flash:
	avrdude -p$(MCU2) -c$(PRGRMR) -P$(PORT) -e -U flash:w:$(BDIR)$(OUTPUT).hex

